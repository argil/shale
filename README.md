# Shale

Shale is a templating language designed to write web documentation. Focused on a hydrated design, shale adds on to the CommonMark standard with unique syntax to reduce your time in an IDE and increase the amount of documentation written.