#include <iostream>
#include <string>
// Program Files
#include "src/includes/CLI_COMMANDS.hpp"


int main(int argc, char *argv[])
{

    if (argc > 1)
    {
        std::string command = argv[1];
        if (command == "setup")
        {
            std::cout << "Heating up Kiln..." << std::endl;
            if (argc > 2)
            {
                std::string query = argv[2];
                kiln::CLICommands::make(query);

                // Build Frontend Files
            }
            else
            {
                std::cout << "Please provide a build path" << std::endl;
            }
        }
        else if (command == "fire")
        {
            if (argc > 3)
            {
                std::string in_file = argv[2];
                std::string out_file = argv[3];
                kiln::CLICommands::build(in_file, out_file);
            }
            else
            {
                std::cerr << "Please provide a input file path and output file path" << std::endl;
            }

            /*else {
                std::cout << "Please provide a build path" << std::endl;
            }*/
        }
        else if (command == "--version")
        {
            std::cout << "Kiln v0.0.205" << std::endl;
        }
        else
        {
            std::cout << "Please check out documentation at" << std::endl << "https://gitlab.com/argil/kiln/-/blob/main/README.md (temp solution)" << std::endl;
        }
    }
    else
    {
        std::cout << "Please check out documentation at" << std::endl << "https://gitlab.com/argil/kiln/-/blob/main/README.md (temp solution)" << std::endl;
    }
    return 0;
}
