#include "./includes/CLI_COMMANDS.hpp"
#include "./includes/Parser.hpp"
#include <iostream>
#include <filesystem>
#include <fstream>
#include <vector>


bool kiln::CLICommands::make(std::string path)
{
    std::vector<std::string> paths = {
        "/public/",
        /*"/src/backend",*/
        "/src/data",
        "/src/frontend"};

    std::vector<bool> results;
    std::fstream output;

    for (const auto i : paths)
    {
        std::filesystem::path target = path + i;
        if (std::filesystem::create_directories(target))
        {
            std::cout << "Building: " << target << std::endl;
            results.push_back(true);
        }
        else
        {
            std::cout << "Failed to build: " << target << std::endl;
            results.push_back(false);
        }
    }

    output.open(path + "/fuel.txt", std::ios::out);
    if (output)
    {
        output << "# Welcome to Kiln! Define Variables Here!" << std::endl;
        output.close();
        std::cout << "Created 'fuel.txt'" << std::endl;
    }
    else
    {
        std::cout << "Failed to open file" << std::endl;
    }
    //Error and Result Checking
    for (auto r : results)
    {
        if (!r)
        {
            return false;
        }
    }

    return true;
}

bool kiln::CLICommands::build(std::string in_file, std::string out_file) {
    std::cout << "Firing the kiln..." << std::endl;
    //Current Working Directory
    std::filesystem::path cwd = std::filesystem::current_path();
    std::error_code ec;
    std::filesystem::path in_file_path(in_file);
    std::filesystem::path out_file_path(out_file);
    if (std::filesystem::is_regular_file(in_file_path) && std::filesystem::is_regular_file(out_file_path)) {
        // std::ifstream target;
        kiln::Parser in_parse(in_file_path);
        try {
           in_parse.validate();
        }
        catch (char e) {
            std::cerr << e;
        }

    }

    if (ec) {
        std::cerr << "Error in file validity: " << ec.message() << std::endl;
    }
    
    
    
    return true;
}