#pragma once

#include <vector>
#include <iostream>
namespace kiln {
	// Dataslice is the container for content between two tokenized charachters
	struct DataSlice {
		int start_line;
		std::string body;
		int end_line;
	};

	
}
