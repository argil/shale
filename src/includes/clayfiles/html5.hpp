#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "../datatypes/Line.hpp"
#include "../Operators.hpp"
#include "data_selection.hpp"

namespace kiln
{
	class HTML5
	{
	public:
		HTML5(std::vector<Line> doc);
		

		void parse();
		

	private:
		std::vector<Line> doc;
		DataSlice parse_layout();
		void parse_layout_string(DataSlice layout);
	};
}