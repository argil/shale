#pragma once
#include <iostream>
#include <vector>
#include <string>

namespace kiln {
	class Operators {
	public:
		static std::vector<std::string> tokenize(std::string const& target, const char deliminator);
	};

}