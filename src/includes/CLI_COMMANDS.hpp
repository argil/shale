#pragma once
#include <iostream>


namespace kiln
{
	class CLICommands
	{
	public:
		// Makes the Setup Files for Kiln.
		static bool make(std::string path);
		static bool build(std::string in_file, std::string out_file);
	};
}
